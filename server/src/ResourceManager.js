/* Start ResourceManager class */
var ResourceManager = Class.extend({
	init: function(resources) 
	{
		for(var i = 0; i<resources; i++) 
		{
			if(resource[i] instanceof Resource) 
			{
				throw "ResourceManager can only load instances that are a sub-class of Resource"
			}
		}
		this.resources = resources;
	},
	load: function(callback) 
	{
		var length = 0;
		var numLoaded = 0;
		
		// Get the total count
		for(var i in this.resources)
		{
			length += Number(this.resources.hasOwnProperty(i));
		}
		
		// Load the individual resources
		for (var i in this.resources) 
		{
			var resource = this.resources[i];
			if(!resource.isLoaded) 
			{
				resource.load(function(success) 
				{
					if(success) 
					{
						numLoaded++;
						if(numLoaded==length) 
						{
							callback(true);
						}
					} else {
						callback(false);
					}
				});
			} else {
				numLoaded++;
				if(numLoaded==length) 
				{
					callback(true);
				}
			}
		}
	},
	get: function(key, callback) 
	{
		var resource = this.resources[key];
		if(resource) {
			if(resource.isLoaded) 
			{
				callback(resource);
			} else {
				this.load(function(success) 
				{ 
					if(success) {
						callback(resource);
					} else {
						callback(false);
					}
				});
			}
		} else {
			callback(false);
		}
	},
	getSync: function(key) 
	{
		var resource = this.resources[key];
		if(resource&&resource.isLoaded) 
		{
			return resource;
		} else {
			throw "Resource not loaded, try calling the asynchronous ResourceManager.get(key, callback) for " + key;
		}
	}
});