var MiniMap = EventReceiver.extend({
	init: function(container, viewport,xResolution,yResolution) {
		this._super();
		this.container = container;
		this.initCanvas();
		this.viewport = viewport;
		this.xResolution = xResolution;
		this.yResolution = yResolution;
		this.sourceMap = this.viewport.map;
		this.elevations = this.getHeightMap();
		
		var _self = this;
		this.events = {
			"mousedown":function(event){_self.onMouseDown(event);}
		};
		this.xpos = 0;
		this.ypos = 0;
		this.height = 200;
		this.width = 200;
	},
	initCanvas: function() {
		var canvas = document.createElement("canvas");
		canvas.id = "miniMap";

		var existing = document.getElementById(canvas.id);
		if(existing) {
			this.container.removeChild(existing);
		}
		this.container.appendChild(canvas);
		this.canvas = canvas;
		this.context = canvas.getContext("2d");
	},
	onMouseDown: function(e) {
		// Get the relative x/y position on the mini-map
		var pageX = e.pageX-this.xpos;
		var pageY = e.pageY-this.ypos;
		
		var scale = this.viewport.scale;
		
		var tileStepX = Config.tileStepX*scale;
		var tileStepY = Config.tileStepY*scale;
		
		// Calculate the scale of the mini-map
		var xScale = this.width/(this.sourceMap.rows.length*tileStepX);
		var yScale = this.height/(this.sourceMap.rows[0].length*tileStepY);
		
		// Convert into full-sized pixel coordinates
		var xTarget = pageX/xScale;
		var yTarget = pageY/yScale;
		
		// Let the scroller do the walking
		this.viewport.panTo(xTarget,yTarget,true);
	},
	getHeightMap: function() {
		var rowLength = this.sourceMap.rows.length;
		var colLength = this.sourceMap.rows[0].length;
		var xRez = this.xResolution;
		var yRez = this.yResolution;
		
		var sqSize = 10;
		var avgDivisor = xRez*yRez;
		var elevationAvg = new Array(rowLength/yRez);
		
		for (var y = 0; y < elevationAvg.length; y++) {
			var elColAvg = new Array(colLength/xRez);
			for (var x = 0; x < elColAvg.length; x++) {
				var yOffset = y*yRez;
				var rowSq = this.sourceMap.elevations.slice(yOffset,yOffset+yRez);
				var total = 0;
				for(var i = 0; i<rowSq.length;i++) {
					var xOffset = x*xRez;
					var colSq = rowSq[i].slice(xOffset,xOffset+xRez);
					for(var j = 0; j<colSq.length;j++) {
						total += colSq[j];
					}
				}
				elColAvg[x] = total/(avgDivisor);
			}
			elevationAvg[y] = elColAvg
		}
		return elevationAvg;
	},
	draw: function(startX,startY,width,height) {
		var scale = this.viewport.scale;
		this.xpos = startX;
		this.ypos = startY;
		this.width = width;
		this.height = height;
		
		//Convenience vars
		var tileStepX = Config.tileStepX*scale;
		var tileStepY = Config.tileStepY*scale;
		
		this.currentBounds = {xmin:startX,xmax:startX+width,ymin:startY,ymax:startY+height};
		var cameraXLocation = this.viewport.location.x;
		var cameraYLocation = this.viewport.location.y;
		var windowTileWidth = this.viewport.getWidth()/tileStepX;
		var windowTileHeight = this.viewport.getHeight()/tileStepY;
		// setting the width/height of the canvas is a shortcut for clearing it
		this.canvas.width  = window.innerWidth;
		this.canvas.height = window.innerHeight;
	
		var windowXTile = Math.floor(cameraXLocation / tileStepX);
		var windowYTile = Math.floor(cameraYLocation / tileStepY);
		
		var xScale = width/(this.sourceMap.rows.length*tileStepX);
		var yScale = height/(this.sourceMap.rows[0].length*tileStepY);
		// TileX and TileY are the map position of the main view camera
		var windowX = windowXTile*tileStepX*xScale;
		var windowY = windowYTile*tileStepY*yScale;
		
		// Draw background
		this.context.globalAlpha = 1;
		this.context.fillStyle = "rgb(255,255,255)";
		this.context.fillRect(startX-2,startY-2,width+4,height+4);
		
		
		// Draw elevation map
		this.context.globalAlpha = 0.65;
		this.context.fillStyle = "rgb(139,69,19)";
		var rowLength = this.elevations.length;
		
		for (var y = 0; y < rowLength; y++) {
			var col = this.elevations[y];
			var colLength = col.length;
			for (var x = 0; x < colLength; x++) {
				var avg = col[x];
				this.context.globalAlpha = avg/Config.maxElevation;
				var stepX = tileStepX*xScale*this.xResolution;
				var stepY = tileStepY*yScale*this.yResolution;
				this.context.fillRect(startX+(stepX*x),startY+(stepY*y),stepX,stepY);
			}
		}
		
		//Draw view window
		this.context.globalAlpha = .40;
		this.context.fillStyle = "rgb(255,255,0)";
		this.context.fillRect(startX+windowX,startY+windowY,windowTileWidth*tileStepX*xScale,windowTileHeight*tileStepY*yScale);
	}
});