/*
* A concrete implementation of the ElevationTransformer class responsible for
* building Mountains
*/
var MountainBuilder = ElevationTransformer.extend({
	init: function(hillCount,minHillHeight,maxHillHeight,mapEdgeInset) {
		this.hillCount = hillCount;
		this.minHillHeight = minHillHeight;
		this.maxHillHeight = maxHillHeight;
		this.mapEdgeInset = mapEdgeInset;
	},
	// Concrete implemention of transform satisfies the ElevationTransformer interface
	// Builds the hills
	transform: function(elevations) {
		var hillCount = this.hillCount;
		var minHillHeight = this.minHillHeight;
		var maxHillHeight = this.maxHillHeight;
		var mapEdgeInset = this.mapEdgeInset;
		
		// Build the number of hills specified
		for (var i = 0; i < hillCount; i++)
		{
			// Pick a random elevation within the threshold
			var hillElevation = Math.floor(Math.random() * maxHillHeight - minHillHeight) + minHillHeight;
			
			// Pick a random Map Coordinate within the Map Edge Inset bounds
			var hillX = Math.floor(Math.random() * elevations[0].length - mapEdgeInset) + mapEdgeInset;
			var hillY = Math.floor(Math.random() * elevations.length - mapEdgeInset) + mapEdgeInset;
			
			// Call the static recursiveDescent method to adjust the elevations outward from the peak
			MountainBuilder.recursiveDescent(elevations,hillY,hillX,hillElevation);
		}
	}
});

// This is how we add a "static" method to the class we instantiated above
// It doesn't really fit as part of the prototype (i.e., as an instance method), so I moved it outside.
MountainBuilder.recursiveDescent = function(elevations,startY,startX,level) {
	// Our lower boundary on the recursion is 0, so only continue if the Level is greater than 0
	if(level>0) 
	{						
		// Ensure the coordinate is within the bounds of the elevation map and higher than the current elevation at that coordinate
		if (startY >= 0 && startY < elevations.length &&
			startX >= 0 && startX < elevations[0].length && 
			level > elevations[startY][startX]) 
		{
			elevations[startY][startX] = level;
			// Visit all coordinates within one step on both axes of the elevation map
			for (var y = -1; y <= 1; y++)
			{
				var thisY = startY + y;
				for (var x = -1; x <= +1; x++)
				{
					var thisX = startX + x;
					// If not the coordinate we are already on, continue recursion one level lower
					if(!(thisX==startX&&thisY==startY)) {
						MountainBuilder.recursiveDescent(elevations,thisY,thisX,level-1);
					}
				}
			}
		} 
	}
}