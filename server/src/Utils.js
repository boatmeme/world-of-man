/* 
	Utility functions and extension of core Javascript prototypes
*/

// We're using the bitwise shift method due to the performance advantages over Math.round() - http://jsperf.com/math-round-vs-hack/3
Math.fastRound = function(num){
    return (0.5 + num) << 0;
};

// Provides a method for clamping a given number value between the provided min/max values
Math.clamp = function(value,min,max){
    return Math.min(Math.max(value, min), max);
};