/* MapBuilder is a static class responsible for building the World map and applying transformations to the MapCells and the Elevation map*/
var MapBuilder = (function() {
	// The publicly accessible method that returns a new Map instance
	function build(mapWidth, mapHeight,resourceManager,elevationTransformers) 
	{
		// Construct the Y dimension array to hold MapCells
		var rows = new Array(mapHeight);
		// Construct the Y dimension array to store the height map
		var elevations = new Array(mapHeight+1);
		
		// For every row...
		for(var i=0;i<rows.length;i++) 
		{
			// Construct the X dimension array for MapCells
			var cols = new Array(mapWidth);
			// For every column...
			for(var j=0;j<cols.length;j++) 
			{
				// Randomly select a TileSet to use for the Grass texture
				var textureId = Math.floor((Math.random()*3)+1);
				// Construct a new MapCell
				var cell = new MapCell("grass");
				cols[j] = cell;							
			}
			// Add the column array to the current row
			rows[i] = cols;
		}
		
		// Initialize the Height map with the possibility for some random variation
		// **We could potentially remove the random variation now that we've implemented ElevationTransformers
		for(var i=0;i<elevations.length;i++) 
		{
			var cols = new Array(mapWidth+1);
			for(var j=0;j<cols.length;j++) 
			{
				var random = Math.random();
				// hillThreshold is the inverse probability of a corner becoming elevated
				// Set to 1 for a perfectly flat elevation.
				// Set to 0.68 for a 32% chance of hills, for example
				var hillThreshold = 1
				var elevation = random>hillThreshold ? 1 : 0;
				cols[j] = elevation;
			}
			elevations[i] = cols;
		}
		
		// Apply the elevation transformations
		runElevationTransformers(elevations,elevationTransformers);			

		// Return the newly created Map
		return new Map(rows,elevations);
	}
	function runElevationTransformers(elevations, elevationTransformers) 
	{
		// If elevationTransformers is not an Array, make it one
		if( Object.prototype.toString.call( elevationTransformers ) !== '[object Array]' ) {
			elevationTransformers = [elevationTransformers];
		}
		
		// Apply each ElevationTransformer to the height map.
		for(var i = 0; i<elevationTransformers.length; i++) 
		{
			var transformer = elevationTransformers[i];
			// Make sure the transformer instance is actually an ElevationTransformer
			if(!transformer instanceof ElevationTransformer) 
			{
				throw "Invalid elevation transformer";
			}
			transformer.transform(elevations);
		}
	}
	// Returns the publicly accessible static methods
	return {
		build: build
	}
})();