/* MouseMapTileSet ecapsulates all of the behavior necessary to determine mouse-overs via the mouse-mapping method */
var MouseMapTileSet = TileSet.extend({
	init: function(url, width, height) 
	{	
		this._super(url,width,height);
		this.canvas = document.createElement("canvas");
		this.context = this.canvas.getContext("2d");
	},
	load: function(callback) 
	{
		var _self = this;
		this._super(function(loadSuccess) {
			if(loadSuccess) {
				_self.canvas.width = _self.image.width;
				_self.canvas.height = _self.image.height;
				_self.context.drawImage(_self.image,0,0);
			}		
			callback(loadSuccess);
		});
	},
	getPixel: function(x,y) {
		if(isNaN(x)||isNaN(y)) {
			return [];
		}
		var imgData=this.context.getImageData(x,y,1,1);
		return imgData.data;
	},
	getMouseOverTileCoords: function(tileId,relX,relY) {
		var coords = this.getTileCoords(tileId);
		var pixels = this.getPixel(coords.x+relX,coords.y+relY);
		
		if(pixels.length>0) {
			
			var mapping = ColorMap.getMapping(pixels);
			if(mapping) {
				return mapping;
			}
		} 
		return false;		
	}
});
/* End MouseMapTileSet class*/


var Color = Class.extend({
	init: function(r,g,b,a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	},
	equals: function(color) {
		if(color instanceof Color) {
			if( this.r == color.r &&
				this.g == color.g &&
				this.b == color.b && 
				this.a == color.a) 
			{
				return true;
			}
		} else if (color.length==4) {
			if( this.r == color[0] &&
				this.g == color[1] &&
				this.b == color[2] && 
				this.a == color[3]) 
			{
				return true;
			}
		}
		return false;
	}
});

var ColorMap = Class.extend({
	init: function(color,x_even,y_even,x_odd,y_odd,tileSet) {
		this.color = color;
		this.x_even = x_even;
		this.y_even = y_even;
		this.x_odd = x_odd;
		this.y_odd = y_odd;
		this.tileSet = tileSet;
	}
});

ColorMap.getMapping = function(pixelArr) {
	for (var key in ColorMap.constants) {
      var map = ColorMap.constants[key];
	  if(map.color.equals(pixelArr)) {
			return map;
	  }
   }
   //Assume all other non-black pixels are part of the center piece of the tile itself
   if( pixelArr[0] > 0&&
	   pixelArr[1] > 0&&
	   pixelArr[2] > 0&&
	   pixelArr[3] > 0) {
		return ColorMap.DEFAULT;
	}
   return false;
}

ColorMap.constants = {};
//Tiles
ColorMap.DEFAULT  		  	= new ColorMap(null,
											0,0, 
											0,0,
											TileSet.centerOverSuffix);
ColorMap.constants.CENTER 	= new ColorMap(new Color(255,255,255,255),
											0, 0, 
											0, 0,
											TileSet.centerOverSuffix);																			
ColorMap.constants.NORTH 	= 	new ColorMap(new Color(255,0,255,255),
											0,-2, 
											0,-2,
											TileSet.centerOverSuffix);
ColorMap.constants.NORTHEAST = new ColorMap(new Color(255,0,0,255),
											0,-1,
											1,-1,
											TileSet.centerOverSuffix);
ColorMap.constants.NORTHWEST = new ColorMap(new Color(0,0,255,255),
											-1,-1,
											0,-1,
											TileSet.centerOverSuffix);											
ColorMap.constants.SOUTHEAST = new ColorMap(new Color(255,255,0,255),
											0,1,
											1,1,
											TileSet.centerOverSuffix);
ColorMap.constants.SOUTHWEST = new ColorMap(new Color(0,255,0,255),
											-1,1,
											0,1,
											TileSet.centerOverSuffix);
ColorMap.constants.SOUTH 	= new ColorMap(new Color(0,255,255,255),
											0,2,
											0,2,
											TileSet.centerOverSuffix);
											
// Corners			
ColorMap.constants.CENTER_NORTH_CORNER 	= new ColorMap(new Color(150,150,150,255),
											0, 0, 
											0, 0,
											TileSet.northOverSuffix);
ColorMap.constants.CENTER_WEST_CORNER 	= new ColorMap(new Color(150,0,0,255),
											0, 0, 
											0, 0,
											TileSet.westOverSuffix);	
ColorMap.constants.CENTER_SOUTH_CORNER 	= new ColorMap(new Color(0,150,150,255),
											0, 0, 
											0, 0,
											TileSet.southOverSuffix);	
ColorMap.constants.CENTER_EAST_CORNER 	= new ColorMap(new Color(150,150,0,255),
											0, 0, 
											0, 0,
											TileSet.eastOverSuffix);	
											
ColorMap.constants.NORTHEAST_WEST_CORNER = new ColorMap(new Color(200,200,0,255),
											0,-1,
											1,-1,
											TileSet.westOverSuffix);
ColorMap.constants.NORTHEAST_SOUTH_CORNER = new ColorMap(new Color(100,255,255,255),
											0,-1,
											1,-1,
											TileSet.southOverSuffix);
ColorMap.constants.SOUTHWEST_EAST_CORNER = new ColorMap(new Color(200,150,200,255),
											-1,1,
											0,1,
											TileSet.eastOverSuffix);
ColorMap.constants.SOUTHWEST_NORTH_CORNER = new ColorMap(new Color(100,0,100,255),
											-1,1,
											0,1,
											TileSet.northOverSuffix);
ColorMap.constants.SOUTHEAST_WEST_CORNER = new ColorMap(new Color(200,0,0,255),
											0,1,
											1,1,
											TileSet.westOverSuffix);
ColorMap.constants.SOUTHEAST_NORTH_CORNER = new ColorMap(new Color(255,100,0,255),
											0,1,
											1,1,
											TileSet.northOverSuffix);	
ColorMap.constants.NORTHWEST_EAST_CORNER = new ColorMap(new Color(200,200,150,255),
											-1,-1,
											0,-1,
											TileSet.eastOverSuffix);
ColorMap.constants.NORTHWEST_SOUTH_CORNER = new ColorMap(new Color(255,0,100,255),
											-1,-1,
											0,-1,
											TileSet.southOverSuffix);


