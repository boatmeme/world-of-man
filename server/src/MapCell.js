/* Represents a distinct X,Y position on a Map */
var MapCell = Class.extend({
	init: function(tileSetKey) 
	{
		this.tileSetKey = tileSetKey;
		this.tileId = 0;
		this.elevationOffset = 0;
	}, 
	draw: function(context, startX, startY, scale) 
	{
		var tileSet = resourceManager.getSync(this.tileSetKey);
		var coords = tileSet.getTileCoords(this.tileId);
		
		var targetX = startX;
		var targetY = startY;
		context.drawImage(tileSet.image,coords.x,coords.y,coords.width,coords.height,targetX,targetY,coords.width*scale,coords.height*scale);		
	},
	setCorners: function(north,east,south,west) 
	{
		var slopes = CornerLevel.getSlopes(north,east,south,west);
		this.tileId = CornerLevel.getTileIndex(slopes);
		this.elevationOffset = slopes.northCornerSlope == CornerLevel.DOWN ? north : 
							   slopes.southCornerSlope == CornerLevel.DOWN ? south : 
							   slopes.easterSlope == CornerLevel.DOWN ? east : 
							   slopes.westCornerSlope == CornerLevel.DOWN ? west : 
							   Math.min(north,south,east,west);
	}
});

// Static function to load MapCell from persitent JSON object
MapCell.rehydrate = function(jsonObj) {
	var cell = new MapCell(jsonObj.tileSetKey);
	cell.tileId = jsonObj.tileId;
	cell.elevationOffset = jsonObj.elevationOffset;
	return cell;
}