/* TileSet ecapsulates all of the data necessary to define a distinct tile sheet image */
var TileSet = Resource.extend({
	init: function(url, width, height) 
	{
		this.tileWidth = width;
		this.tileHeight = height;
		this.image = new Image();
		this._super(url);
		this.tileCoordsCache = [];
	},
	load: function(callback) 
	{
		if(this.isLoaded) {
			callback(true);
		} else {
			var _self = this;
			this.image.onerror = function() {
				this.isLoaded = false;
				callback(false);
			}
			this.image.onload = function() {
				_self.isLoaded = true;
				callback(true);
			}
			this.image.src = this.resourceUrl;
		}
	},
	getTileCoords: function(index) 
	{
		if(!this.isLoaded) 
		{
			throw("Tile texture has not been initialized");
		}
		
		// Check cache
		var coords = this.tileCoordsCache[index];
		
		// If not cached, then do the calculations and store in cache;
		if(!coords) 
		{
			var tileY = Math.floor(index / (this.image.width / this.tileWidth));
			var tileX = index % (this.image.width / this.tileWidth);
			coords = {x:tileX * this.tileWidth,
				y:tileY * this.tileHeight,
				width: this.tileWidth,
				height: this.tileHeight};
			this.tileCoordsCache[index] = coords;
		}
		return coords;
	}
});

TileSet.centerOverSuffix = "_over";
TileSet.westOverSuffix = "_west";
TileSet.eastOverSuffix = "_east";
TileSet.northOverSuffix = "_north";
TileSet.southOverSuffix = "_south";
/* End TileSet class*/