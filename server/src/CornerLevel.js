var CornerLevel = (function() {
	var LEVEL = "l";
	var UP ="u";
	var DOWN =  "d";
	var tileIndex = {};
	
	tileIndex[UP+UP+UP+UP] = 0;
	tileIndex[UP+LEVEL+LEVEL+LEVEL] = 1;
	tileIndex[LEVEL+UP+LEVEL+LEVEL] = 2;
	tileIndex[LEVEL+LEVEL+LEVEL+UP] = 3;
	tileIndex[LEVEL+LEVEL+UP+LEVEL] = 4;
	
	tileIndex[LEVEL+UP+UP+UP] = 6;
	tileIndex[UP+LEVEL+UP+UP] = 7;
	tileIndex[UP+UP+UP+LEVEL] = 8;
	tileIndex[UP+UP+LEVEL+UP] = 9;
	
	tileIndex[LEVEL+LEVEL+UP+UP] = 11;
	tileIndex[LEVEL+LEVEL+DOWN+DOWN] = 12;
	tileIndex[UP+UP+LEVEL+LEVEL] = 13;
	tileIndex[LEVEL+LEVEL+UP+UP] = 14;
	
	tileIndex[LEVEL+LEVEL+UP+DOWN] = 16;
	tileIndex[LEVEL+LEVEL+DOWN+UP] = 17;
	tileIndex[DOWN+UP+LEVEL+LEVEL] = 18;
	tileIndex[UP+DOWN+LEVEL+LEVEL] = 19;
	
	tileIndex[LEVEL+UP+LEVEL+UP] = 21;
	tileIndex[LEVEL+UP+UP+LEVEL] = 22;
	tileIndex[UP+LEVEL+LEVEL+UP] = 23;
	tileIndex[UP+LEVEL+UP+LEVEL] = 24;

	function getLevel(num) 
	{
		if(num==1) 
		{
			return UP;
		}
		if(num==-1) 
		{
			return DOWN;
		}
		if(num==0) 
		{
			return LEVEL;
		}
		//console.log("Invalid jump between elevation levels");
		//throw "Invalid jump between elevation levels";
	}
	function getSlopes(north,east,south,west) 
	{
		var max = Math.max(north,east,south,west);
		
		var slopes = {northCornerSlope: getLevel((north-max)+1),
					  southCornerSlope: getLevel((south-max)+1),
					  eastCornerSlope: getLevel((east-max)+1),
					  westCornerSlope: getLevel((west-max)+1)
		}
		return slopes;
	}
	
	function getTileIndex(slopes) 
	{
		return tileIndex[slopes.northCornerSlope+slopes.southCornerSlope+slopes.eastCornerSlope+slopes.westCornerSlope];
	}
	return {
		getSlopes:getSlopes,
		getTileIndex:getTileIndex,
		UP:UP,
		DOWN:DOWN,
		LEVEL:LEVEL
	}
})();