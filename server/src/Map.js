/* Map encapsulates the collection of MapCells and Elevations that comprise a WorldMap */
var Map = Class.extend({
	// Constructor takes two, two-dimensional arrays representing rows and columns of MapCells
	// and rows and columns of Elevation points
	init: function(mapArray, elevations) 
	{
		if(mapArray.length<1||mapArray[0].length<1) { 
			throw "Must initialize map with a two dimensional array of Rows, and Columns of MapCells"; 
		}
		if(elevations.length<1||mapArray[0].elevations<1) { 
			throw "Must initialize map with a two dimensional array of Rows, and Columns of Elevations"; 
		}
		this.rows = mapArray;
		this.elevations = elevations;
		this.width = this.rows[0].length;
		this.height = this.rows.length;
		// Initialize the MapCell elevations for the entire map
		this.adjustElevations(0, 0, this.width, this.height);
	},	
	// Adjusts the corners/elevation of a given square of the map
	adjustElevations: function(startx,starty,xsize,ysize) 
	{
		//startx and starty represent the starting coords of the area to recalculate
		//xsize, ysize represent the size of the area to recalculate
		if(starty+ysize>this.height||startx+xsize>this.width) 
		{
			throw "adjustElevations: Area cannot be larger that the MapCell grid)";
		}
		for(var i=starty;i<starty+ysize;i++) 
		{
			for(var j=startx;j<startx+xsize;j++) 
			{
				var south = this.getSouthernElevation(j,i);
				var north = this.getNorthernElevation(j,i);
				var east = this.getEasternElevation(j,i);
				var west = this.getWesternElevation(j,i);
				this.rows[i][j].setCorners(north,east,south,west);
			}
		}
	},
	// Helper method for getting the elevations relative to a MapCell's X,Y position
	getNorthernElevation: function(x,y) 
	{
		// Odd rows are different
		if(y%2==1) 
		{
			return this.elevations[y-1][x+1];
		}
		if(y>0) 
		{
			return this.elevations[y-1][x];
		}
		return 0;
	}
	,
	getSouthernElevation: function(x,y) 
	{
		// Odd rows are different
		if(y%2==1) 
		{
			return this.elevations[y+1][x+1];
		}
		return this.elevations[y+1][x];
	}
	,
	getEasternElevation: function(x,y) 
	{
		return this.elevations[y][x+1];
	}
	,
	getWesternElevation: function(x,y) 
	{
		return this.elevations[y][x];
	}
});

Map.rehydrate = function(jsonObj) {
	var rowLength = jsonObj.rows.length;
	var mapArray = new Array(rowLength);
	
	for(var i = 0;i<rowLength;i++) {
		var col = jsonObj.rows[i];
		var colLength = col.length;
		var colArray = new Array(colLength);
		for(var j = 0;j<colLength;j++) {
			colArray[j] = MapCell.rehydrate(col[j]);
		}
		mapArray[i] = colArray;
	}
	
	var elevations = jsonObj.elevations;
	var map = new Map(mapArray,elevations);
	return map;
}