/*
* Defines the interface for ElevationTransformers
*
* Since there is no way to define interfaces in Javascript, this seems a decent way to approximate them.
*/
var ElevationTransformer = Class.extend({
	init: function() {
		//No-op
	},
	// The "Abstract" transform method that sub-classes must implement. Enforces contract by throwing an Error if the 
	// method remains unimplemented
	transform: function(elevations) {
		throw "Must provide a concrete implementation of transform() function"
	}
});