/*
* The base class for all Resources
*/
var Resource = Class.extend({
	init: function(url) {
		this.resourceUrl = url;
		this.isLoaded = false;
	},
	load: function(callback) {
		throw "Resource sub-class must provide a concrete implementation of load()"
	}
});