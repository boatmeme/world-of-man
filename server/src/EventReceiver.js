var EventReceiver = Class.extend({
	init: function() {
		this.events = {};
		this.currentBounds = {xmin:0,xmax:0,ymin:0,ymax:0};
	},
	dispatchEvent: function(e) {
		var x = e.pageX;
		var y = e.pageY;
		if(x>=this.currentBounds.xmin&&x<=this.currentBounds.xmax
			&& y>=this.currentBounds.ymin&&y<=this.currentBounds.ymax) {
			var callback = this.events[e.type];
			if(callback) {
				callback(e);
			}
			return true;
		}
		return false;
	},
});