// Represents a view of the World Map and the position of the Camera within that world
var Viewport = EventReceiver.extend({
	init: function(container,map) {
		this._super();
		
		this.container = container;
		this.initCanvas();
		
		this.location = {x:0,y:0};
		this.map = map
		var _self = this;
		this.scale = 1;
		
		this.scroller = scroller = new Scroller(function(left,top,zoom){
			_self.moveCamera(left,top);
			_self.scale = zoom;
		}, {
			minZoom: 0.25,
			maxZoom: 3,
			zooming: true,
			locking: false,
			animationDuration: 1000
		});
		
		scroller.setDimensions(window.innerWidth, window.innerHeight, Config.mapWidth*Config.tileStepX, Config.mapHeight*Config.tileStepY);
		this.events = {
			"touchstart":this.onTouchStart,
			"touchcancel":this.onTouchCancel,
			"touchmove":this.onTouchMove,
			"touchend":this.onTouchEnd,
			"mousedown":this.onMouseDown,
			
			"mousemove":function(event){
				_self.onMouseMove(event)
			},
			"mouseup":this.onMouseUp,
			"mouseover":this.onMouseOver,
			"mousewheel":function(event) {
				_self.onMouseWheel(event);
			},
			"DOMMouseScroll":function(event) {
				_self.onMouseWheel(event);
			}
		
		};
		this.mouseX = 0;
		this.mouseY = 0;
		
		var screenMap = new Array();
		
		this.requireViewportRedraw = true;
		this.requireTemplateRedraw = false;
	},
	initCanvas: function() {
		// Viewport Canvas
		var canvas = document.createElement("canvas");
		canvas.id = "viewport";

		var existing = document.getElementById(canvas.id);
		if(existing) {
			this.container.removeChild(existing);
		}
		this.container.appendChild(canvas);
		this.canvas = canvas;
		this.context = canvas.getContext("2d");
		
		//Template Canvas
		var canvas = document.createElement("canvas");
		canvas.id = "template";

		existing = document.getElementById(canvas.id);
		if(existing) {
			this.container.removeChild(existing);
		}
		this.container.appendChild(canvas);
		this.templateCanvas = canvas;
		this.templateContext = canvas.getContext("2d");
	},
	onTouchStart: function(e) {
		scroller.doTouchStart(e.touches, e.timeStamp);
	},
	onTouchMove: function(e) {
		scroller.doTouchMove(e.touches, e.timeStamp, e.scale);
	},
	onTouchEnd: function(e) {
		scroller.doTouchEnd(e.timeStamp);
	},
	onMouseDown: function(e) {
		scroller.doTouchStart([{
			pageX: e.pageX,
			pageY: e.pageY
		}], e.timeStamp);
	},
	onMouseMove: function(e) {
		scroller.doTouchMove([{
			pageX: e.pageX,
			pageY: e.pageY
		}], e.timeStamp);
		// Highlight square
		this.mouseX = e.pageX;
		this.mouseY = e.pageY;
		this.requireTemplateRedraw = true;
	},
	onMouseUp: function(e) {
		scroller.doTouchEnd(e.timeStamp);
	},
	onMouseOver: function(e) {
		console.log(e);
	},
	onTouchCancel: function(e) {
		scroller.doTouchEnd(e.timeStamp);
	},
	onMouseWheel: function(e) {
		var delta = e.wheelDelta || -e.detail; 
		var zoomFactor = Config.zoomFactor/100;
		var change = delta > 0 ? 1-zoomFactor : 1+zoomFactor;
		//scroller.doMouseZoom(delta, e.timeStamp, e.pageX, e.pageY);
		scroller.zoomTo(this.scale * change, true, e.pageX - scroller.__clientLeft, e.pageY - scroller.__clientTop);
	},
	getWidth: function() {
		return this.canvas.width;
	},
	getHeight: function() {
		return this.canvas.height;
	},
	clearViewportCanvas: function() {
		var width = this.canvas.width = window.innerWidth;
		var height = this.canvas.height = window.innerHeight;
		this.context.clearRect(0, 0, width, height)
	},
	clearTemplateCanvas: function() {
		var width = this.templateCanvas.width = window.innerWidth;
		var height = this.templateCanvas.height = window.innerHeight;
		this.templateContext.clearRect(0, 0, width, height)
	},
	draw: function() {
		if(this.requireViewportRedraw) {
			this.drawViewportLayer();
			this.requireViewportRedraw = false;
		} 
		if (this.requireTemplateRedraw) {
			this.drawTemplateLayer();
			this.requireTemplateRedraw = false;
		}
	},
	// Draw the viewport layer, the map tiles
	drawViewportLayer: function() {
			var scale = this.scale;
			// Convenience vars
			var tileStepX = Config.tileStepX*scale;
			var tileStepY = Config.tileStepY*scale;
			var baseOffsetY = Config.baseOffsetY*scale;
			var baseOffsetX = Config.baseOffsetX*scale;
			var heightTileOffset = Config.heightTileOffset*scale;
			var oddRowXOffset = Config.oddRowXOffset*scale;
		
			// Clear the canvas
			this.clearViewportCanvas();
			
			var width = this.getWidth();
			var height = this.getHeight();
			
			// This is necessary for the EventReceiver stuff. Probably should move this somewhere else.
			this.currentBounds = {xmin:0,xmax:width,ymin:0,ymax:height};
			
			// Figure out how many Tiles fit in the viewport - with some extra padding added
			var numCellsDown = (height/tileStepY)+20;
			var numCellsAcross = (width/tileStepX)+2;
	
			// The first Map X coordinate that will be painted in the Viewport
			var firstX = Math.floor(this.location.x / tileStepX);
			// The first Map Y coordinate that will be painted in the Viewport
			var firstY = Math.floor(this.location.y / tileStepY);
			
			// The offsets for determining how far back to start drawing the partial Map X,Ys that 
			// hang over the edges of the viewport
			var offsetX = Math.floor(this.location.x % tileStepX);
			var offsetY = Math.floor(this.location.y % tileStepY);
			
			// For the number of squares that fit vertically in the viewport...
			for (var y = 0; y < numCellsDown; y++) {
				var mapY = y + firstY;
				
				// Account for the isometric-style offset on odd rows
				var rowOffset = 0;
				if ((mapY) % 2 == 1)
					rowOffset = oddRowXOffset;
					
				// For the number of squares that fit horizontally in the viewport
				for (var x = 0; x < numCellsAcross; x++) {
					var mapX = x + firstX;
					// Get the MapCell at this Map coordinate and draw it
					var cell = this.map.rows[mapY][mapX];
					var drawX = (x*tileStepX)-offsetX+baseOffsetX+rowOffset;
					var drawY = (y*tileStepY)-offsetY + baseOffsetY - (cell.elevationOffset * heightTileOffset);
					cell.draw(this.context,drawX,drawY,scale);
				}
			}
	},
	// Draw the template layer - the mouseover tiles and interactivity graphics
	drawTemplateLayer: function() {
		// Clear the canvas
		this.clearTemplateCanvas();
		
		var mouseX = this.mouseX;
		var mouseY = this.mouseY;
		// Fail fast if we're not actually mousing over the bounds of the template viewport
		if(mouseX < 0 || mouseY < 0) {
			return;
		}
		
		var mouseOverCoords;
		var scale = this.scale;
		// Convenience vars
		var tileWidth = Config.tileWidth*scale;
		var tileHeight = Config.tileHeight*scale;
		var tileStepX = Config.tileStepX*scale;
		var tileStepY = Config.tileStepY*scale;
		var baseOffsetY = Config.baseOffsetY*scale;
		var baseOffsetX = Config.baseOffsetX*scale;
		var heightTileOffset = Config.heightTileOffset*scale;
		var oddRowXOffset = Config.oddRowXOffset*scale;
		
		var width = this.getWidth();
		var height = this.getHeight();
		
		// Figure out how many Tiles fit in the viewport - with some extra padding added
		var numCellsDown = (height/tileStepY)+20;
		var numCellsAcross = (width/tileStepX)+2;

		// The first Map X coordinate that will be painted in the Viewport
		var firstX = Math.floor(this.location.x / tileStepX);
		// The first Map Y coordinate that will be painted in the Viewport
		var firstY = Math.floor(this.location.y / tileStepY);
		
		// The offsets for determining how far back to start drawing the partial Map X,Ys that 
		// hang over the edges of the viewport
		var offsetX = Math.floor(this.location.x % tileStepX);
		var offsetY = Math.floor(this.location.y % tileStepY);
		
		// For the number of squares that fit vertically in the viewport...
outer: for (var y = 0; y < numCellsDown; y++) {
			var mapY = y + firstY;
			
			// Account for the isometric-style offset on odd rows
			var rowOffset = 0;
			if ((mapY) % 2 == 1)
				rowOffset = oddRowXOffset;
				
			// For the number of squares that fit horizontally in the viewport
			for (var x = 0; x < numCellsAcross; x++) {
				var mapX = x + firstX;
				// Get the MapCell at this Map coordinate and draw it
				var cell = this.map.rows[mapY][mapX];
				var drawX = (x*tileStepX)-offsetX+baseOffsetX+rowOffset;
				var drawY = (y*tileStepY)-offsetY + baseOffsetY - (cell.elevationOffset * heightTileOffset);
				
				/* Start MouseOver Magic*/
				// It may be possible to encapsulate this code in some other class
				// All we're doing here is calculating the MouseOver information. We will draw it after we've found the coordinates
				
				if(mouseX>drawX && mouseX<drawX+tileWidth
					&&mouseY>drawY && mouseY<drawY+tileHeight) {
					// Get the mouse coords relative to the current tile for use on its mouse map
					var localX = Math.fastRound((mouseX-drawX)/scale);
					var localY = Math.fastRound((mouseY-drawY)/scale);
					
					// Get the mousemap resource for this tile
					var tileSet = resourceManager.getSync(cell.tileSetKey+"_map");
					// Get the ColorMap object for the given tile-relative mouse coordinates
					var relMouseOverCoords = tileSet.getMouseOverTileCoords(cell.tileId,localX,localY);
					
					if(relMouseOverCoords) {
						// ColorMap object provides both odd and even relative coordinates
						var relX = mapY % 2 == 0 ? relMouseOverCoords.x_even : relMouseOverCoords.x_odd;
						var relY = mapY % 2 == 0 ? relMouseOverCoords.y_even : relMouseOverCoords.y_odd;
						// Stash an anonymous object for use in drawing the MouseOver below
						mouseOverCoords = {x:relX+x,y:relY+y, tileSet: relMouseOverCoords.tileSet};
						mouseFound = true;
						
						/* Display mousemap tile for debug purposes */
						if(Config.showMouseMap) {
							var coords = tileSet.getTileCoords(cell.tileId);
							this.templateContext.drawImage(tileSet.image,coords.x,coords.y,coords.width,coords.height,drawX,drawY,coords.width*scale,coords.height*scale);	
							this.templateContext.fillStyle = "black";
							this.templateContext.fillText(mapX+","+mapY, drawX, drawY);
						}
						break outer;
					}
				}
			}
		}
		
		// Draw MouseOver if we found one
		if(mouseOverCoords) {
			var x = mouseOverCoords.x;
			var y = mouseOverCoords.y;
			var tileSuffix = mouseOverCoords.tileSet;
			
			// From here, the code looks remarkably similar to the above code for drawing the map
			var mapX = x + firstX;
			var mapY = y + firstY;
			
			var rowOffset = 0;
			if ((mapY) % 2 == 1)
				rowOffset = oddRowXOffset;
				
			var cell = this.map.rows[mapY][mapX];
			var drawX = (x*tileStepX)-offsetX+baseOffsetX+rowOffset;
			var drawY = (y*tileStepY)-offsetY + baseOffsetY - (cell.elevationOffset * heightTileOffset);
			var tileSet = resourceManager.getSync(cell.tileSetKey+tileSuffix);
			var coords = tileSet.getTileCoords(cell.tileId);
			
			var targetX = drawX;
			var targetY = drawY;
			
			// Draw it at 35% alpha
			this.templateContext.globalAlpha = Config.templateAlpha;
			this.templateContext.drawImage(tileSet.image,coords.x,coords.y,coords.width,coords.height,targetX,targetY,coords.width*scale,coords.height*scale);
			this.templateContext.globalAlpha = 1;
			
			/* Uncomment to display coordinate info during debug */
			/*
			this.context.fillStyle = "black";
			this.context.fillText(mapX+","+mapY, targetX, targetY);		
			*/
		}	
		
	},
	// Helper function returns a new location object literal that is constrained to the bounds of the World Map
	clampCoords: function(x,y) {
		var scale = this.scale;
		var tileStepX = Config.tileStepX*scale;
		var tileStepY = Config.tileStepY*scale;
		var width = this.canvas.width;
		var height = this.canvas.height;
		
		// Figure out how many Tiles fit in the viewport - with some extra padding added
		var numCellsDown = (height/tileStepY)+20;
		var numCellsAcross = (width/tileStepX)+2;
		
		var xmin = 0;
		var ymin = 0;
		var xmax = (this.map.width-numCellsAcross)*tileStepX;
		var ymax = (this.map.height-numCellsDown)*tileStepY;
		
		//Clamp the new coordinates based on the min/max coordinate values - uses custom Math.clamp() extension (see Utils.js)
		return { 
			x:Math.clamp(x, xmin, xmax),
			y:Math.clamp(y, ymin, ymax)
		};
	},
	moveCamera: function(x, y) {
		this.location = this.clampCoords(x,y);
		// We moved the camera, so we must redraw
		this.requireViewportRedraw = true;
		this.requireTemplateRedraw = true;
	},
	panTo: function(absoluteX,absoluteY,animate) {
		this.mouseX = -1;
		this.mouseY = -1;
		this.scroller.scrollTo(absoluteX,absoluteY,animate);
	},
	pan: function(relativeX,relativeY) {
		this.panTo(this.location.x+relativeX,this.location.y+relativeY,false);
	},
	panLeft: function(step) {
		this.pan(-step,0);
	},
	panRight: function(step) {
		this.pan(step,0);
	},
	panUp: function(step) {
		this.pan(0, -step);
	},
	panDown: function(step) {
		this.pan(0, step);
	}
});